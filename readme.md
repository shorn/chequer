Chequer is a Unity asset that does static analysis of the current Unity scene.

See [the Chequer wiki](https://bitbucket.org/shorn/chequer/wiki/Home "The Chequer wiki") for more information, screenshots, etc.