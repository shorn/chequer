﻿using System;

namespace Chequer {

[AttributeUsage(AttributeTargets.Field)]
public class NotNullAttribute : Attribute {
  
}

[AttributeUsage(AttributeTargets.Field)]
public class NotBlankAttribute : Attribute {
  
}

}