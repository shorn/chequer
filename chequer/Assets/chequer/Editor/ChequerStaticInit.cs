﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Chequer {

/// Do I need to do the HideFlags.HideAndDontSave thing for this class?
/// It's not ref'd by the scene, so maybe it will get GC'd?
/// 
/// TODO:SBT look into using the Highlighter functionality somehow
[InitializeOnLoad]
public static class ChequerStaticInit {
  public static SceneCheckGlobalState State { get; private set; }
  public static HierarchyChecker Checker { get; private set; }
  public static HierarchyRenderer HierarchyRenderer { get; private set; }
  public static ChequerAssetManager AssetManager { get; private set; }
  
  private static int _lastPerSecondTickTime;

  static ChequerStaticInit(){
    State = ScriptableObject.CreateInstance<SceneCheckGlobalState>();
    State.LoadState();

    EditorLogging.LoggingEnabled = State.logging;
    EditorLogging.Timebug("Chequer started");


    AssetManager = ChequerAssetManager.CreateInstance();
    HierarchyRenderer = new HierarchyRenderer(AssetManager);
    Checker = new HierarchyChecker();

    EditorApplication.update += EditorUpdate;
    EditorApplication.hierarchyWindowItemOnGUI +=
      HierarchyRenderer.OnHierarchyWindowItemOnGUI;

    // set to current time - first tick will be in 1 second's time 
    // set to 0 - first tick will be as soon as EditorUpdate gets called
    _lastPerSecondTickTime = Environment.TickCount;
  }

  public static void EditorUpdate(){
    EditorEveryTick();

    // Can't use Time.realtimeSinceStartup, because it gets reset
    // when you exit playmode. 
    // TickCount will roll over to 0 after 25 days, should be ok.
    if( Environment.TickCount - _lastPerSecondTickTime > State.interval ){
      Editor1PerInterval();
      // imagine the tick processing took > 1 second, we want the next tick
      // to be once second AFTER this tick finished processing 
      _lastPerSecondTickTime = Environment.TickCount;
    }
  }


  private static void EditorEveryTick(){
    CheckerMainWindow.EditorEveryTick();
  }

  private static void Editor1PerInterval(){
    CheckerMainWindow.Editor1PerInterval();

    if( State.autoUpdate ){
      IList<CheckFailure> failures = Checker.CheckForFailures();
      HierarchyRenderer.UpdateState(failures);
      EditorApplication.RepaintHierarchyWindow();
    }
  }

  public static void DoManualCheck(){
    IList<CheckFailure> failures = Checker.CheckForFailures();

    if (failures.Count > 0){
      EditorLogging.Warn("Found these issues...\n" + 
        Checker.ConvertFailuresToString(failures));
    }
    else{
      EditorLogging.Info("Found no issues.");
    }

    if (State.manualHierarchy){
      HierarchyRenderer.UpdateState(failures);
      EditorApplication.RepaintHierarchyWindow();
    }
    else{
      // not sure if we should touch the hierarchy window even for this if user
      // has selected that we shouldn't update the hierarchy
      // HierarchyRenderer.Clear();
    }
  }

  public static void ClearNodeState(){
    EditorLogging.Debug("Clearing the hierarchy window");
    HierarchyRenderer.Clear();
    EditorApplication.RepaintHierarchyWindow();
  }
}

/// These values survive asset reload, playmode toggle and even restart of 
/// Unity, but when the window is closed, they're gone if not saved to 
/// EditorPrefs. 
/// 
/// Note that these prefs are global at the moment - if you have two completely
/// separate projects that both use Chequer, they will share their preferences.
/// Don't know how to fix yet, dunno how to get the project name.
/// And maybe sharing the settings is the right thing to do anyway?
[Serializable]
public class SceneCheckGlobalState : ScriptableObject {
  public const string UserOptionKey = "Chequer.userOption";
  public const string UserOptionDefault = "defaultValue";

  public const string IntervalKey = "Chequer.interval";
  public const int IntervalDefault = 1000;

  public const string AutoUpdateKey = "Chequer.autoUpdate";
  public const bool AutoUpdateDefault = true;

  public const string LoggingKey = "Chequer.logging";
  public const bool LoggingDefault = false;

  public const string ManualHierarchyKey = "Chequer.manualHierarcy";
  public const bool ManualHierarchyDefault = true;

  public string userOption;

  public int interval;
  public bool autoUpdate;
  public bool logging;
  public bool manualHierarchy;

  public SceneCheckGlobalState(){
    // this apparently tells Unity not to GC this object (Unity always uses
    // the scene as the root of reachability, but Windows aren't referenced 
    // from the scene). 
    hideFlags = HideFlags.HideAndDontSave;
  }

  public void LoadState(){
    userOption = EditorPrefs.GetString(UserOptionKey, UserOptionDefault);
    interval = EditorPrefs.GetInt(IntervalKey, IntervalDefault);
    autoUpdate = EditorPrefs.GetBool(AutoUpdateKey, AutoUpdateDefault);
    logging = EditorPrefs.GetBool(LoggingKey, LoggingDefault);
    manualHierarchy = EditorPrefs.GetBool(
      ManualHierarchyKey, ManualHierarchyDefault);
  }

  public void SaveState(){
    SaveString(UserOptionKey, userOption, UserOptionDefault);
    SaveInt(IntervalKey, interval, IntervalDefault);
    SaveBool(AutoUpdateKey, autoUpdate, AutoUpdateDefault);
    SaveBool(LoggingKey, logging, LoggingDefault);
    SaveBool(ManualHierarchyKey, manualHierarchy, ManualHierarchyDefault);
  }

  private void SaveString(string key, string value, string defaultValue){
    // need to deal carefully with default value, so that you can change
    // the defautl value in code without having to worry about previous 
    // default values that may have been saved to prefs.
    //  
    // There's still an issue where the user intentionally wanted the 
    // default value, without knowing that is was the default, and when the 
    // default changes in code, they still want the original default value.  
    // I reckon that'd be best dealth with by doco though.
    if( value == defaultValue ){
      // deal with case where user set a non-default, saved, but then set 
      // back to default value
      EditorPrefs.DeleteKey(key);
    }
    else{
      EditorPrefs.SetString(key, value);
    }
  }

  private void SaveInt(string key, int value, int defaultValue){
    if( value == defaultValue ){
      EditorPrefs.DeleteKey(key);
    }
    else{
      EditorPrefs.SetInt(key, value);
    }
  }

  private void SaveBool(string key, bool value, bool defaultValue){
    if( value == defaultValue ){
      EditorPrefs.DeleteKey(key);
    }
    else{
      EditorPrefs.SetBool(key, value);
    }
  }
}
}
