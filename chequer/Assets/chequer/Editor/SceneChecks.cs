using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace Chequer {

public class CheckFailure {
  public CheckFailure(string message, MonoBehaviour target){
    Message = message;
    Target = target;
  }

  public string Message { get; private set; }
  public MonoBehaviour Target { get; private set; }

  public virtual string GetFullTargetString(){
    return FormatTargetSpecifier(true);
  }

  public virtual string GetSimpleTargetString(){
    return FormatTargetSpecifier(false);
  }

  public string FormatTargetSpecifier(bool showPath){
    if( showPath ){
      return String.Format("{0}/{1}",
        GetPath(Target.transform),
        Target.GetType().Name);
    }
    else{
      return String.Format("{0}/{1}",
        Target.gameObject.name,
        Target.GetType().Name);
    }
  }

  public static string GetPath(Transform current){
    if( current.parent == null ){
      return "/" + current.name;
    }
    return GetPath(current.parent) + "/" + current.name;
  }
}

abstract class SceneGameObjectCheck {
  public abstract IEnumerable<CheckFailure> Check(MonoBehaviour target);
}


class FieldCheckFailure : CheckFailure {
  public FieldCheckFailure(
    string message, 
    MonoBehaviour target, 
    FieldInfo fieldInfo) 
  : base(message, target){
    FieldInfo = fieldInfo;
  }

  public FieldInfo FieldInfo { get; private set; }

  public override string GetFullTargetString(){
    return String.Format("{0}.{1}", 
      FormatTargetSpecifier(true), 
      FieldInfo.Name);
  }

  public override string GetSimpleTargetString(){
    return String.Format("{0}.{1}", 
      Target.GetType().Name, 
      FieldInfo.Name);
  }
}

class NullReferenceCheck : SceneGameObjectCheck {
  public override IEnumerable<CheckFailure> Check(MonoBehaviour target){
    var failures = new List<CheckFailure>();
    ReflectionUtils.VisitAnnotatedFields<NotNullAttribute>(
      target,
      (field, attr) => {
        var fieldValue = field.GetValue(target);
        if( fieldValue == null ){
          failures.Add(new FieldCheckFailure(
            "value was null", target, field));
        }
      });

    return failures;
  }
}

class NotBlankCheck : SceneGameObjectCheck {
  public override IEnumerable<CheckFailure> Check(MonoBehaviour target){
    var failures = new List<CheckFailure>();
    ReflectionUtils.VisitAnnotatedFields<NotBlankAttribute>(
      target,
      (field, attr) => {
        object fieldValue = field.GetValue(target);
        if( !(fieldValue is string) ){
          return;
        }
        if( String.IsNullOrEmpty((string) fieldValue) ){
          failures.Add(new FieldCheckFailure(
            "value was blank", target, field));
        }
      });

    return failures;
  }
}
}