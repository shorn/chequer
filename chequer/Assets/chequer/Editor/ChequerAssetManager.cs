﻿using System.Security.Permissions;
using Chequer;
using UnityEditor;
using UnityEngine;
using System.Collections;

/// ScriptableObject so that we can call MonoScript.FromScriptableObject().
/// 
/// Can't be in the namespace because then AssetDatabase.GetAssetPath() will
/// not return the correct path, don't know why.
/// 
/// Class is designed to sit in the parent of the "icons" directory, so that
/// it can find them by relative path.
public class ChequerAssetManager : ScriptableObject {

  /// This class gets constructed a lot for some reason, it has something to do 
  /// with asset reloading when a script changes.
  /// I think each instance that has ever been created is being deserialised 
  /// (before the InitializeOnLoad method is called), then we end up creating 
  /// another by calling this method again, with then gets deserialised after 
  /// next asset reload.
  /// I think because it's ScriptableObject and is created with 
  /// CreateInstance(), but I don't know how to deal with it.
  /// Maybe the global state class is also getting lots of instances too?
  public static ChequerAssetManager CreateInstance(){
    return CreateInstance<ChequerAssetManager>();
  }

  public ChequerAssetManager(){
//    EditorLogging.TimeLog("ChequerAssetManager.ctor(): " + GetInstanceID());
  }

  public void OnEnable(){
    // this is to get rid of the "asset has been leaked X times" message
    // not sure if/how it relates to my issues with multiple instances
    hideFlags = HideFlags.HideAndDontSave;
  }

  public void OnDisable(){
  }

  public void OnDestroy(){
  }

  /// <param name="iconName">filename without extension, no "/icon/" prefix, 
  /// no file suffix, e.g. "error-bright-16".
  /// </param>
  public Texture2D LoadIcon(string iconName){
    string path = AssetDatabase.GetAssetPath(
      MonoScript.FromScriptableObject(this));
//    EditorLogging.TimeLog("path: {0}", path);
    path = path.Substring(0, path.LastIndexOf('/'));

    // TODO:SBT deal with failure to find icon
    
    return (Texture2D) AssetDatabase.LoadAssetAtPath(
      path + "/icon/"+iconName+".png", typeof(Texture2D));
  }


}
