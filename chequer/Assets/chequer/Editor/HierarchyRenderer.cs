﻿using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;
using Chequer;

namespace Chequer {

public class HierarchyRenderer { 
  public Dictionary<int, HierarchyNodeState> nodeStates = 
    new Dictionary<int, HierarchyNodeState>();

  private Texture2D iconFailure;
  private Texture2D iconFailureParent;


  public HierarchyRenderer(ChequerAssetManager assetManager){
    iconFailure = assetManager.LoadIcon("error-bright-16");
    iconFailureParent = assetManager.LoadIcon("error-dim-16");
  }

  public void UpdateState(IList<CheckFailure> failures){
    nodeStates = ConvertFailuresToNodes(failures);
  }

  private static Dictionary<int, HierarchyNodeState> ConvertFailuresToNodes(
    IList<CheckFailure> failures)
  {
    var newMap = new Dictionary<int, HierarchyNodeState>();
    foreach( var checkFailure in failures ){
      int instanceId = checkFailure.Target.GetInstanceID();
      if( !newMap.ContainsKey(instanceId) ){
        newMap[instanceId] = new HierarchyNodeState();
      }

      // this is the MB (monobehaviour)
      newMap[instanceId].failed = true;
      newMap[instanceId].failures.Add(checkFailure);

      // assumption: MB always has an associated GO
      GameObject parentGameObject = checkFailure.Target.gameObject;

      int parentInstanceId = parentGameObject.GetInstanceID();
      if( !newMap.ContainsKey(parentInstanceId) ){
        newMap[parentInstanceId] = new HierarchyNodeState();
      }
      // this is the parent GO of the MB
      newMap[parentInstanceId].failed = true;
      newMap[parentInstanceId].failures.Add(checkFailure);

      Transform pathTransform = parentGameObject.transform.parent;
      while( pathTransform != null ){
        int pathInstanceId = pathTransform.gameObject.GetInstanceID();
        if( !newMap.ContainsKey(pathInstanceId) ){
          newMap[pathInstanceId] = new HierarchyNodeState();
        }
        newMap[pathInstanceId].childFailed = true;

        pathTransform = pathTransform.parent;
      }


    }

    return newMap;
  }

  public void OnHierarchyWindowItemOnGUI(int instanceId, Rect selection){
//    Debug.Log(instanceId + ": " + selection);
//    GUI.Box(selection, "");

    if( !nodeStates.ContainsKey(instanceId) ){
      return;
    }
     
    var failureNode = nodeStates[instanceId];

    float iconWidth = selection.height;
    var r = new Rect(
      selection.x + selection.width - iconWidth,
      selection.y,
      iconWidth,
      selection.height);

    GUIContent failIndicator;
    if( failureNode.failed ){
      var buf = new StringBuilder();
      foreach( CheckFailure failure in failureNode.failures ){
        buf.AppendLine(
          string.Format(
            "{0} for {1}",
            failure.Message, failure.GetSimpleTargetString()));
        buf.AppendLine();
      }
      failIndicator = new GUIContent(
        iconFailure, 
        buf.ToString());
    }
    else{
      failIndicator = new GUIContent(
        iconFailureParent, 
        "some child object has a failure");
    }
    
    GUI.Label(r, failIndicator);
  }

  public void Clear(){
    nodeStates = new Dictionary<int, HierarchyNodeState>();
  }
}
}