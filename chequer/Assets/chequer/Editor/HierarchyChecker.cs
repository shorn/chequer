using System;
using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Chequer {

public class HierarchyChecker { 

  public HierarchyChecker(){
  }

  public String ConvertFailuresToString(IList<CheckFailure> failures){
    var buf = new StringBuilder();
    foreach( var checkFailure in failures ){
      buf.AppendLine(String.Format("    failed: {0}, on {1}",
          checkFailure.Message, checkFailure.GetFullTargetString()));
    }
    return buf.ToString();
  }



  public IList<CheckFailure> CheckForFailures(){
    var objects = (MonoBehaviour[])
      Object.FindObjectsOfType(typeof(MonoBehaviour));

    var checks = new List<SceneGameObjectCheck>();
    checks.Add(new NullReferenceCheck());
    checks.Add(new NotBlankCheck());
    var failures = new List<CheckFailure>();
    for( int i = 0; i < objects.Length; i++ ){
      MonoBehaviour iObject = objects[i];

      foreach( var check in checks ){
        var currentFailures = check.Check(iObject);
        failures.AddRange(currentFailures);
      }
    }
    return failures;
  }

}

public class HierarchyNodeState {
  public bool failed = false;
  public bool childFailed = false;
  public List<CheckFailure> failures = new List<CheckFailure>();
}

}
