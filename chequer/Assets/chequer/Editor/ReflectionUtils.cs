﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Chequer {

static public class ReflectionUtils {
  static public IEnumerable<FieldInfo> GetAllFields(this Type t){
    if( t == null ){
      return Enumerable.Empty<FieldInfo>();
    }

    const BindingFlags flags = 
      BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | 
      BindingFlags.Instance | BindingFlags.DeclaredOnly;
    return t.GetFields(flags).Concat(GetAllFields(t.BaseType));
  }

  public static IEnumerable<FieldInfo> GetAllFields(this MonoBehaviour t){
    if( t == null ){
      return Enumerable.Empty<FieldInfo>();
    }

    return GetAllFields(t.GetType());
  }


  public static void VisitAnnotatedFields<TAttribute>(
    MonoBehaviour target,
    Action<FieldInfo, TAttribute> closure)
    where TAttribute : Attribute
  {
    IEnumerable<FieldInfo> fieldInfos = target.GetAllFields();
    foreach( var iField in fieldInfos ){
      foreach( var jAttr in iField.GetCustomAttributes(false) ){
        if( jAttr.GetType().IsAssignableFrom(typeof(TAttribute)) ){
          closure.Invoke(iField, (TAttribute) jAttr);
        }
      }
    }
  }
}
}