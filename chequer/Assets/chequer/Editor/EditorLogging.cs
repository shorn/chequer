﻿using System;
using UnityEngine;
using System.Collections;

namespace Chequer {
public class EditorLogging : ScriptableObject {
  static EditorLogging(){
    LoggingEnabled = false;
  }

  public static bool LoggingEnabled { get; set; }

  public static void Debug(string s, params object[] args){
    if( !LoggingEnabled ) return;
    UnityEngine.Debug.Log(String.Format(s, args));
  }

  /// Debug with a timestamp
  public static void Timebug(string s, params object[] args){
    if( !LoggingEnabled ) return;
    Debug(String.Format(Timestamp + " " + s, args));
  }

  public static void Info(string s, params object[] args){
    UnityEngine.Debug.Log(String.Format(s, args));
  }

  public static void Warn(string s, params object[] args){
    UnityEngine.Debug.LogWarning(String.Format(s, args));
  }

  public static void Error(string s, params object[] args){
    UnityEngine.Debug.LogError(String.Format(s, args));
  }

  public static string Timestamp{
    get { return DateTime.Now.ToString("HH:mm:ss.fff"); }
  }
}
}