﻿using System;
using UnityEditor;
using UnityEngine;

namespace Chequer {

[Serializable]
internal class HierarchyCheckWindowState {
  public string tickContent = "flibble";
  public bool showClockEveryTick = false;
}

public class CheckerMainWindow : EditorWindow {
  private static CheckerMainWindow _window;

  [SerializeField] private HierarchyCheckWindowState windowState;

  /// Theory: gets called first time GetWindow is called with this type
  /// and when the window was already open and assets are reloaded?
  public CheckerMainWindow(){
    if( _window != null ){
      EditorLogging.Error(
        "there was already a window registered in the static",
        _window);
    }

    _window = this;
  }

  [MenuItem("Window/Chequer", false, 1000)]
  private static void Init(){
//    EditorLogging.TimeLog("SceneCheckWindow.Init()");
    EditorWindow.GetWindow(typeof(CheckerMainWindow), false, "Chequer");
  }

  public void OnGUI(){
//    DoTheDisplayTick();
//    DoTheTickOption();
//    DoTheUserOption();
    EditorGUILayout.BeginVertical();
    DoTheLoggingOption();
    EditorGUILayout.EndVertical();

    EditorGUILayout.Space();

    EditorGUILayout.BeginVertical();
    DoTheIntervalOption();
    DoTheAutoUpdateOption();
    EditorGUILayout.EndVertical();

    EditorGUILayout.Space();

    EditorGUILayout.BeginVertical();
    DoCheckButton();
    DoManualHierarchyOption();
    EditorGUILayout.EndVertical();

    EditorGUILayout.Space();

    EditorGUILayout.BeginVertical();
    DoClearButton();
    EditorGUILayout.EndVertical();
  }

  public void OnEnable(){
    if( windowState == null ){
      windowState = new HierarchyCheckWindowState();
    }
  }

  public void OnDestroy(){
    if( _window == this ){
      _window = null;
    }
  }

  public void DoTheDisplayTick(){
    EditorGUILayout.TextField("Tick time", windowState.tickContent);
  }

  public void DoTheTickOption(){
    windowState.showClockEveryTick = EditorGUILayout.Toggle(
      "Update clock every tick",
      windowState.showClockEveryTick);
  }

  public void DoTheUserOption(){
    SceneCheckGlobalState state = ChequerStaticInit.State;
    if( state == null ){
      EditorGUILayout.SelectableLabel("CheckingState not initialised!");
      return;
    }

    string newValue = EditorGUILayout.TextField(
      "User option", state.userOption);
    state.userOption = newValue;
    state.SaveState();
  }

  public void DoTheIntervalOption(){
    SceneCheckGlobalState state = ChequerStaticInit.State;
    if( state == null ){
      EditorGUILayout.SelectableLabel("CheckingState not initialised!");
      return;
    }

    int newValue = EditorGUILayout.IntField(
      new GUIContent(
        "Refresh interval (ms)", 
        "Time in milliseconds between each refresh of the hierarchy window" ),
      state.interval); 
    state.interval = newValue;
    state.SaveState();
  }

  public void DoTheAutoUpdateOption(){
    SceneCheckGlobalState state = ChequerStaticInit.State;
    if( state == null ){
      EditorGUILayout.SelectableLabel("CheckingState not initialised!");
      return;
    }

    bool oldValue = state.autoUpdate;
    bool newValue = EditorGUILayout.Toggle(
      new GUIContent(
        "Auto Update", 
        "Update the hierarchy window every refresh interval, basically the" +
          " same as clicking 'Do Check` every interval millis" ),
      state.autoUpdate); 
    state.autoUpdate = newValue;
    state.SaveState();

    if( oldValue && !newValue ){
      // if we just turned off auto updating, then clear the window
      EditorLogging.Timebug("clearing the hierarchy because autoUpdate has" +
        " been turned off");
      ChequerStaticInit.ClearNodeState();
    }
  }

  public void DoManualHierarchyOption(){
    SceneCheckGlobalState state = ChequerStaticInit.State;
    if( state == null ){
      EditorGUILayout.SelectableLabel("CheckingState not initialised!");
      return;
    }

    bool newValue = EditorGUILayout.Toggle(
      new GUIContent(
        "Manual Update Hierarchy", 
        "Controls if the manual check button updates the hierarchy" ),
      state.manualHierarchy); 
    state.manualHierarchy = newValue;
    state.SaveState();

  }

  public void DoTheLoggingOption(){
    SceneCheckGlobalState state = ChequerStaticInit.State;
    if( state == null ){
      EditorGUILayout.SelectableLabel("CheckingState not initialised!");
      return;
    }

    bool oldValue = state.logging;
    bool newValue = EditorGUILayout.Toggle(
      new GUIContent(
        "Logging", 
        "Toggle debug logging of  the Chequer asset" ),
      state.logging); 
    state.logging = newValue;
    state.SaveState();

    if (oldValue != newValue){
      EditorLogging.LoggingEnabled = state.logging;
    }
  }

  public void DoCheckButton(){
    if( ChequerStaticInit.Checker == null ){
      EditorGUILayout.SelectableLabel("HierarchyChecker not initialised!");
      return;
    }

    EditorGUILayout.BeginHorizontal();
    EditorGUILayout.PrefixLabel("Check failures manually");
    bool doCheckPressed =
      GUILayout.Button(
        new GUIContent(
          "Do Check", 
          "Run all the checks on the current scene and show problems in" +
            " the hierarchy window."), 
        GUILayout.ExpandWidth(false));
    EditorGUILayout.EndHorizontal();

    if( doCheckPressed ){
      ChequerStaticInit.DoManualCheck();
    }
  }

  public void DoClearButton(){
    if( ChequerStaticInit.Checker == null ){
      EditorGUILayout.SelectableLabel("HierarchyChecker not initialised!");
      return;
    }

    EditorGUILayout.BeginHorizontal();
    EditorGUILayout.PrefixLabel("Clear hierarcy window");
    bool doClear =
      GUILayout.Button(
        new GUIContent(
          "Clear Failures", 
          "Reset any previously detected failures (will clear out the" +
            " hierarchy window"), 
        GUILayout.ExpandWidth(false));
    EditorGUILayout.EndHorizontal();

    if( doClear ){
      ChequerStaticInit.ClearNodeState();
    }
  }

  public static void EditorEveryTick(){
    if (_window == null){
      return;
    }

    if( _window.windowState.showClockEveryTick ){
      _window.windowState.tickContent = EditorLogging.Timestamp;
      _window.Repaint();
    }
  }

  public static void Editor1PerInterval(){
    if (_window == null){
      return;
    }

    if( !_window.windowState.showClockEveryTick ){
      _window.windowState.tickContent = EditorLogging.Timestamp;
      _window.Repaint();
    }

  }
}

}